import base64
import json
import os
from itertools import product, chain, starmap
from functools import wraps

from PyQt5 import QtCore
from PyQt5.QtWidgets import QMainWindow, QInputDialog, QMessageBox
from designer_qt5.Ui_MainWindow import Ui_MainWindow

from DataClasses.Preset import Preset
from WindowClasses.AboutDialog import AboutDialog
from helpers.constants import (
    BAND_NAMES,
    DIALSTEPS,
    FREQUENCY_DEFAULTS,
    GRAPH_FREQUS,
    GRAPH_HEIGHT,
    LADSPA_LABEL,
    LADSPA_LIBRARY,
    LADSPA_SINK_NAME,
    MAX_GAIN,
    MIN_GAIN,
    PARAMEQ_MMAP_MAGIC,
    PARAM_TYPES,
)
from helpers.functions import (
    TEXT_TO_VALUE_FUNCTIONS,
    VALUE_TO_DIAL_STEPS_FUNCTIONS,
    VALUE_TO_TEXT_FUNCTIONS,
    dialSteps2frequency,
    dialSteps2gain,
    dialSteps2q,
    frequency2dialSteps,
    gain2dialSteps,
    pulse_get_hw_sinks,
    pulse_get_sink_volume,
    pulse_insert_module,
    pulse_move_sink_inputs,
    pulse_set_sink_volume,
    q2dialSteps,
)
from helpers.plotting import (
    coeffsBAHighShelf,
    coeffsBALowShelf,
    coeffsBAPeaking,
    frequency_response,
    ResponsePlot,
)
from helpers.eq_interfacing import LinkedEQ
from helpers.dependency_checking import qt_app_import_check
from helpers.modify_pa_config import (
    insert_in_pa_config,
    remove_from_pa_config,
    CouldNotReadGlobalConfigError,
    CouldNotWriteBackupConfigError,
    CouldNotWriteLocalConfigError,
)


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setupUi(self)
        ## pulsectl import check
        qt_app_import_check(self, "pulsectl")
        # pa config modifiers
        self._insert_in_pa_config = self._error_handle_modify_pa(insert_in_pa_config)
        self._remove_from_pa_config = self._error_handle_modify_pa(
            remove_from_pa_config
        )
        # instance variable
        self._prohibitLineEditUpdates = False
        self._disablePresetsComboboxHandler = False
        self._loadedPresetName = ""
        self._eq = LinkedEQ()
        self._pulseClient = None
        self._pulseaudio_moduleindex = -1
        self._last_pulseaudio_hw_sink = ""
        self._last_pulseaudio_default_sink = ""
        self._isInserted = False
        self._checkInserted()
        self._eq.setupMmaps()
        self._doNotDirty = False
        self._needApply = False
        self._needRedraw = False
        self._bypassed = False
        # push button bindings
        self.pushButtonAbout.clicked.connect(self.onShowAboutDialog)
        self.pushButtonBypass.clicked.connect(self.onBypass)
        self.pushButtonInsert.clicked.connect(self.onInsert)
        self.pushButtonSavePreset.clicked.connect(self.onSavePreset)
        # set maximum on dials
        for suffix in BAND_NAMES:
            for t in "Frequency", "Gain", "Q":
                getattr(self, "dial%s%s" % (t, suffix)).setMaximum(DIALSTEPS)
        # presets combo box
        self.comboBoxPresets.currentIndexChanged.connect(self.onPresetComboBoxSelected)
        # setup graphing things
        self.labelFrGraph.hide()  # hide placeholder label

        self.plot = ResponsePlot(GRAPH_HEIGHT, GRAPH_FREQUS)
        self.mainVerticalLayout.insertWidget(1, self.plot.mplCanvas)

        def _create_changed_function(param_type, band_name):
            def changed_function():
                self._on_dial_changed(param_type, band_name)

            return changed_function

        def _create_line_edit_function(param_type, band_name):
            def line_edit_function():
                self._on_line_edit_changed(param_type, band_name)

            return line_edit_function

        for param_type, band_name in product(PARAM_TYPES, BAND_NAMES):
            # bind changed events of dial
            changed_func = _create_changed_function(param_type, band_name)
            q_dial = getattr(self, f"dial{param_type}{band_name}")
            q_dial.valueChanged.connect(changed_func)
            # bind changed events of lineedit
            line_edit_func = _create_line_edit_function(param_type, band_name)
            q_line_edit = getattr(self, f"lineEdit{param_type}{band_name}")
            q_line_edit.editingFinished.connect(line_edit_func)

        # gain slider setup
        self.sliderGain.setMaximum(int(round(MAX_GAIN * 20)))
        self.sliderGain.mouseDoubleClickEvent = self.onGainSliderDoubleClicked
        self.sliderGain.valueChanged.connect(self.onGainSliderChanged)
        # setup QSettings object and read ini settings
        self._settings = QtCore.QSettings(
            "PulseaudioParametricEq", "PulseaudioParametricEq"
        )
        self._presets = {}
        self._readIniSettings()
        # setup auto-apply timer
        self._timerApplyEq = QtCore.QTimer(self)
        self._timerApplyEq.setInterval(50)
        # noinspection PyUnresolvedReferences
        def apply_eq_if_needed():
            if self._needApply:
                if self._bypassed:
                    self._eq.make_neutral()
                else:
                    self._eq.applyEq(self._uiParams())
                self._needApply = False

        self._timerApplyEq.timeout.connect(apply_eq_if_needed)
        self._timerApplyEq.start()
        # setup update ui timer
        self._needRedraw = True
        self._timerUpdateUi = QtCore.QTimer(self)
        self._timerUpdateUi.setInterval(50)
        # noinspection PyUnresolvedReferences
        self._timerUpdateUi.timeout.connect(self._updateFrGraph)
        self._timerUpdateUi.start()

    def closeEvent(self, evt):
        """write ini settings before close"""
        self._writeIniSettings()

    def onBypass(self):
        """bypass checked state was changed"""
        self._bypassed = self.pushButtonBypass.isChecked()
        if self._bypassed:
            self.pushButtonBypass.setText("Bypassed")
        else:
            self.pushButtonBypass.setText("Bypass")
        self._needApply = True

    def onGainSliderChanged(self):
        """gain slider was changed, update label"""
        gain = MIN_GAIN + self.sliderGain.value() / 10.0
        self.labelGain.setText("Gain: %0.1fdB" % gain)
        self._needRedraw = True
        self._needApply = True

    # noinspection PyUnusedLocal
    def onGainSliderDoubleClicked(self, ignored):
        """gain slider was double clicked, change to half value"""
        self.sliderGain.setValue(self.sliderGain.maximum() / 2)

    def onInsert(self):
        """insert eq ladspa module and persist into ini"""
        hw_sinks = pulse_get_hw_sinks()
        hw_sink_descriptions = map(lambda x: x["description"], hw_sinks)
        if self._isInserted:
            idx = 0
            for i, hw_sink_dict in enumerate(hw_sinks):
                if hw_sink_dict["name"] == self._last_pulseaudio_default_sink:
                    idx = i
                    break
            dlg = QInputDialog()
            hw_sink_desc, ok = dlg.getItem(
                self,
                "Choose pulseaudio default sink...",
                "Master sinks:",
                hw_sink_descriptions,
                idx,
                False,
            )
            if not ok:
                return
            hw_sink = hw_sinks[0]["name"]
            for hw_sink_dict in hw_sinks:
                if hw_sink_dict["description"] == hw_sink_desc:
                    hw_sink = hw_sink_dict["name"]
                    break
            self.pushButtonInsert.setText("Remove")
            self._last_pulseaudio_default_sink = hw_sink
            self.pushButtonInsert.setText("Insert")
            self._eq._mmaps = []
            oldVolume = pulse_get_sink_volume(LADSPA_SINK_NAME)
            os.system("pactl set-default-sink %s" % hw_sink)

            pulse_move_sink_inputs(hw_sink)

            os.system("pactl unload-module %s" % self._pulseaudio_moduleindex)
            pulse_set_sink_volume(hw_sink, oldVolume)
            self._pulseaudio_moduleindex = -1
            self._remove_from_pa_config()
            self._isInserted = False
        else:
            idx = 0
            for i, hw_sink_dict in enumerate(hw_sinks):
                if hw_sink_dict["name"] == self._last_pulseaudio_hw_sink:
                    idx = i
                    break
            dlg = QInputDialog()
            hw_sink_desc, ok = dlg.getItem(
                self,
                "Choose pulseaudio master sink...",
                "Master sinks:",
                hw_sink_descriptions,
                idx,
                False,
            )
            if not ok:
                return
            hw_sink = hw_sinks[0]["name"]
            for hw_sink_dict in hw_sinks:
                if hw_sink_dict["description"] == hw_sink_desc:
                    hw_sink = hw_sink_dict["name"]
                    break
            self.pushButtonInsert.setText("Remove")
            self._last_pulseaudio_hw_sink = hw_sink
            oldVolume = pulse_get_sink_volume(hw_sink)

            pulse_insert_module(LADSPA_SINK_NAME, hw_sink)

            pulse_set_sink_volume(hw_sink, 1.0)
            pulse_set_sink_volume(LADSPA_SINK_NAME, oldVolume)
            self._checkInserted()
            if self._isInserted:
                self._insert_in_pa_config(
                    self._uiParams(),
                    LADSPA_SINK_NAME,
                    LADSPA_LIBRARY,
                    LADSPA_LABEL,
                    PARAMEQ_MMAP_MAGIC,
                    hw_sink,
                )
                self._eq.setupMmaps()
                self._needApply = True
            else:
                self._remove_from_pa_config()

    def onPresetComboBoxSelected(self):
        """a preset was selected in the combo box, load it"""
        if self._disablePresetsComboboxHandler:
            return
        name = self.comboBoxPresets.itemText(self.comboBoxPresets.currentIndex())
        if name == "":
            return
        self._doNotDirty = True
        self._loadPresetValues(self._presets[name])
        self._doNotDirty = False
        self._needApply = True
        self._needRedraw = True

    def onSavePreset(self):
        """save a preset with given name"""
        dlg = QInputDialog()
        name, ok = dlg.getText(
            self, "Save preset as...", "Preset name:", text=self._loadedPresetName
        )
        if ok:
            preset = self._valuesToPreset()
            preset.name = name
            self._presets[name] = preset
            self._loadedPresetName = name
            self._populatePresetDropdown()

    @staticmethod
    def onShowAboutDialog():
        """show about dialog"""
        dlg = AboutDialog()
        dlg.exec_()

    def _checkInserted(self):
        """
        check if ladspa module with param eq is already inserted
        if so, note module id of it
        """
        cmd = (
            """pacmd list | grep -B2 -m1 'argument:.*%s' | grep '    index: ' | sed 's/    index: //g'"""
            % LADSPA_SINK_NAME
        )
        ret = os.popen(cmd).read().replace("\n", "")
        if ret == "":
            self._isInserted = False
            self.pushButtonInsert.setText("Insert")
            self._pulseaudio_moduleindex = -1
        else:
            self._isInserted = True
            self.pushButtonInsert.setText("Remove")
            self._pulseaudio_moduleindex = int(ret)

    def _dirty(self):
        """enable apply switch"""
        if self._needApply and self._needRedraw:
            return
        if self._doNotDirty:
            return
        self._needApply = True
        self._needRedraw = True
        if self._loadedPresetName != "":
            self._loadedPresetName = ""
            preset = self._valuesToPreset()
            self._presets[""] = preset
            self._populatePresetDropdown()

    def _loadPresetValues(self, preset):
        """load a given preset instance into inputs"""
        self._loadedPresetName = preset.name
        self._doNotDirty = True
        for suffix in BAND_NAMES:
            freq = getattr(preset, "frequency%s" % suffix)
            dial = getattr(self, "dialFrequency%s" % suffix)
            dial.setValue(0)
            dial.setValue(1)
            dial.setValue(frequency2dialSteps(freq))
            gain = getattr(preset, "gain%s" % suffix)
            dial = getattr(self, "dialGain%s" % suffix)
            dial.setValue(0)
            dial.setValue(1)
            dial.setValue(gain2dialSteps(gain))
            q = getattr(preset, "q%s" % suffix)
            dial = getattr(self, "dialQ%s" % suffix)
            dial.setValue(0)
            dial.setValue(1)
            dial.setValue(q2dialSteps(q))
        self.sliderGain.setValue(int(round((MAX_GAIN + preset.gain) * 10)))
        self._doNotDirty = False
        self._needRedraw = True
        self._needApply = True

    def _error_handle_modify_pa(self, func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except CouldNotReadGlobalConfigError:
                QMessageBox().critical(
                    self, "FATAL ERROR", "Could not read /etc/pulse/default.pa !"
                )
            except CouldNotWriteBackupConfigError:
                QMessageBox().critical(
                    self,
                    "FATAL ERROR",
                    "Could not write ~/.config/pulse/default.pa.backup_by_parameq !",
                )
            except CouldNotWriteLocalConfigError:
                QMessageBox().critical(
                    self, "FATAL ERROR", "Could not write ~/.config/pulse/default.pa !"
                )

        return wrapper

    def _on_dial_changed(self, param_type, band_name):
        """Handle changed dial."""
        self._dirty()
        lineEdit = getattr(self, f"lineEdit{param_type}{band_name}")
        dial = getattr(self, f"dial{param_type}{band_name}")
        value_to_text_function = VALUE_TO_TEXT_FUNCTIONS[param_type]
        lineEdit.setText(value_to_text_function(dial.value()))

    def _on_line_edit_changed(self, param_type, band_name):
        if self._prohibitLineEditUpdates:
            return
        self._dirty()
        lineEdit = getattr(self, f"lineEdit{param_type}{band_name}")
        dial = getattr(self, f"dial{param_type}{band_name}")

        text_to_value_function = TEXT_TO_VALUE_FUNCTIONS[band_name]

        text = lineEdit.text()
        try:
            value = text_to_value_function(text)
        except ValueError:
            DEFAULTS = {
                "Frequency": FREQUENCY_DEFAULTS[band_name],
                "Gain": 0,
                "Q": 1,
            }
            value = DEFAULTS[param_type]

        lineEdit.setText("%d Hz" % value)

        self._prohibitLineEditUpdates = True

        value_to_dial_steps_function = VALUE_TO_DIAL_STEPS_FUNCTIONS[param_type]
        dial.setValue(value_to_dial_steps_function(value))

        self._prohibitLineEditUpdates = False

    def _populatePresetDropdown(self):
        """(re)populate presets dropdown menu"""
        self._disablePresetsComboboxHandler = True
        self.comboBoxPresets.clear()
        selected = -1
        i = 0
        for name in sorted(self._presets.keys()):
            self.comboBoxPresets.addItem(name)
            if name == self._loadedPresetName:
                selected = i
            i += 1
        if selected > -1:
            self.comboBoxPresets.setCurrentIndex(selected)
        self._disablePresetsComboboxHandler = False

    def _readIniSettings(self):
        """read ini settings"""
        s = self._settings
        self._bypassed = s.value("main/bypassed", "false") == "true"
        self.pushButtonBypass.setChecked(self._bypassed)
        self._last_pulseaudio_hw_sink = s.value("main/last_pulseaudio_hw_sink", "")
        self._last_pulseaudio_default_sink = s.value(
            "main/last_pulseaudio_default_sink", ""
        )
        # current preset
        currentPreset = Preset()
        try:
            jsonString = base64.b64decode(s.value("main/currentPreset", "")).decode(
                "ascii"
            )
            currentPreset.unserialize(jsonString)
        except json.JSONDecodeError:
            pass
        self._loadedPresetName = currentPreset.name
        self._presets[currentPreset.name] = currentPreset
        # presets
        for k in s.allKeys():
            if k.startswith("presets/"):
                name = k.replace("presets/", "")
                jsonString = base64.b64decode(s.value(k)).decode("ascii")
                preset = Preset()
                preset.unserialize(jsonString)
                self._presets[name] = preset
        # always reinit flat preset
        preset = Preset()
        self._presets[preset.name] = preset
        # load values of current preset
        self._populatePresetDropdown()
        self._loadPresetValues(currentPreset)
        self._populatePresetDropdown()

    def _bands_coeffs(self):
        def band_dial_parameters(band_name):
            frequency_dial = getattr(self, f"dialFrequency{band_name}")
            gain_dial = getattr(self, f"dialGain{band_name}")
            q_dial = getattr(self, f"dialQ{band_name}")

            frequency = dialSteps2frequency(frequency_dial.value())
            gain = dialSteps2gain(gain_dial.value())
            q = dialSteps2q(q_dial.value())

            return frequency, gain, q

        def band_dial_coeffs(band_name):
            COEF_FUNCTIONS = {
                "Low": coeffsBALowShelf,
                "Param1": coeffsBAPeaking,
                "Param2": coeffsBAPeaking,
                "Param3": coeffsBAPeaking,
                "High": coeffsBAHighShelf,
            }
            b, a = COEF_FUNCTIONS[band_name](*band_dial_parameters(band_name))
            return b, a

        return map(band_dial_coeffs, BAND_NAMES)

    def _updateFrGraph(self):
        """update frequency response graph"""
        if not self._needRedraw:
            return

        freq_response = frequency_response(self._bands_coeffs(), GRAPH_FREQUS)

        gain = self.sliderGain.value() / 10.0 + MIN_GAIN
        gain_factor = pow(10, gain / 20.0)

        total_response = freq_response * gain_factor

        self.plot.update_graph(total_response, gain)

        self._needRedraw = False

    def _uiParams(self):
        """return all 16 ui parameters as floats"""

        def get_param_float_value(band_name, param_type):
            line_edit = getattr(self, f"lineEdit{param_type}{band_name}")
            return float(line_edit.text().partition(" ")[0])

        # BAND_NAMES and PARAM_TYPES entry order matters
        band_param_values = starmap(
            get_param_float_value, product(BAND_NAMES, PARAM_TYPES)
        )
        slider_gain_value = self.sliderGain.value() / 10.0 + MIN_GAIN
        return tuple(chain(band_param_values, (slider_gain_value,)))

    def _valuesToPreset(self):
        """make Preset from current values on UI"""
        preset = Preset()
        preset.name = self._loadedPresetName
        for suffix in BAND_NAMES:
            dial = getattr(self, "dialFrequency%s" % suffix)
            setattr(preset, "frequency%s" % suffix, dialSteps2frequency(dial.value()))
            dial = getattr(self, "dialGain%s" % suffix)
            setattr(preset, "gain%s" % suffix, dialSteps2gain(dial.value()))
            dial = getattr(self, "dialQ%s" % suffix)
            setattr(preset, "q%s" % suffix, dialSteps2q(dial.value()))
        preset.gain = self.sliderGain.value() / 10.0 + MIN_GAIN
        return preset

    def _writeIniSettings(self):
        """write ini settings"""
        s = self._settings
        s.setValue("main/bypassed", self._bypassed and "true" or "false")
        s.setValue("main/last_pulseaudio_hw_sink", self._last_pulseaudio_hw_sink)
        s.setValue(
            "main/last_pulseaudio_default_sink", self._last_pulseaudio_default_sink
        )
        preset = self._valuesToPreset()
        jsonString = preset.serialize()
        s.setValue("main/currentPreset", base64.b64encode(jsonString.encode("utf-8")))
        # store presetsf
        toDelete = filter(lambda x: x.startswith("presets/"), s.allKeys())
        for k in toDelete:
            s.remove(k)
        for name, preset in self._presets.items():
            jsonString = preset.serialize()
            s.setValue(
                "presets/%s" % name, base64.b64encode(jsonString.encode("utf-8"))
            )
