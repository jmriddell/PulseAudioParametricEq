"""
Pulseaudio Parametric Equalizer GUI
=======
Main python (startup) file

(c) 2018- Jürgen Herrmann, t-5@t-5.eu

GIT: https://gitlab.com/t-5/PulseaudioParametricEq.git
"""

from qt5_t5darkstyle import darkstyle_css
import sys
from PyQt5.QtWidgets import QApplication
from WindowClasses.MainWindow import MainWindow


def main():
    app = QApplication(sys.argv)
    app.setStyleSheet(darkstyle_css())
    form = MainWindow()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
