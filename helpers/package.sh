#!/bin/bash

cd ..
VERSION=`cat .previous_version`
echo -n "Version (previous version was $VERSION: "
read VERSION

rm -f builds/*.deb

sudo cat control.in | sed "s#Version: _VERSION_#Version: ${VERSION}#" > debian/DEBIAN/control

sudo cp -f pulseaudio-parametric-eq debian/usr/bin
sudo cp -f DataClasses/*.py debian/usr/lib/pulseaudio-parametric-eq/DataClasses
sudo cp -f designer_qt5/*.py debian/usr/lib/pulseaudio-parametric-eq/designer_qt5
sudo cp -f designer_qt5/res/Icon.png debian/usr/share/pixmaps/pulseaudio-parametric-eq.png
sudo cp -f helpers/*.py debian/usr/lib/pulseaudio-parametric-eq/helpers
sudo cp -f PAPEq_rc/*.py debian/usr/lib/pulseaudio-parametric-eq/PAPEq_rc
sudo cp -f WindowClasses/*.py debian/usr/lib/pulseaudio-parametric-eq/WindowClasses
sudo cp -f licence.txt debian/usr/lib/pulseaudio-parametric-eq
sudo cp -f pulseaudio_parameq.py debian/usr/lib/pulseaudio-parametric-eq

sudo chmod o+rX -R debian/*

sudo chown -R root.root debian/
sudo dpkg --build debian && \
    mv debian.deb builds/pulseaudio-parametric-eq_${VERSION}_all.deb && \
    aptly repo add t-5 builds && \
    echo -n "$VERSION" > .previous_version

sudo chown -R jh.jh debian/
