from math import sin, cos, pi, pow, sqrt
from functools import reduce

import numpy
from scipy import signal
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from helpers.constants import SR


def coeffsBAHighShelf(f, g, q):
    """return filter coefficients of high shelf filter"""
    w0 = 2.0 * pi * f / SR
    alpha = sin(w0) / (2.0 * q)
    A = pow(10, g / 40.0)
    # fmt: off
    b0 =      A*((A+1.0) + (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha)
    b1 = -2.0*A*((A-1.0) + (A+1.0)*cos(w0))
    b2 =      A*((A+1.0) + (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha)
    a0 =         (A+1.0) - (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha
    a1 =    2.0*((A-1.0) - (A+1.0)*cos(w0))
    a2 =         (A+1.0) - (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha
    # fmt: on
    return (b0, b1, b2), (a0, a1, a2)


def coeffsBALowShelf(f, g, q):
    """return filter coefficients of low shelf filter"""
    w0 = 2.0 * pi * f / SR
    alpha = sin(w0) / (2.0 * q)
    A = pow(10, g / 40.0)
    # fmt: off
    b0 =     A*( (A+1.0) - (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha)
    b1 = 2.0*A*( (A-1.0) - (A+1.0)*cos(w0))
    b2 =     A*( (A+1.0) - (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha)
    a0 =         (A+1.0) + (A-1.0)*cos(w0) + 2.0*sqrt(A)*alpha
    a1 =  -2.0*( (A-1.0) + (A+1.0)*cos(w0))
    a2 =         (A+1.0) + (A-1.0)*cos(w0) - 2.0*sqrt(A)*alpha
    # fmt: on
    return (b0, b1, b2), (a0, a1, a2)


def coeffsBAPeaking(f, g, q):
    """return filter coefficients of peaking eq filter"""
    w0 = 2.0 * pi * f / SR
    alpha = sin(w0) / (2.0 * q)
    A = pow(10, g / 40.0)
    # fmt: off
    b0 =  1.0 + alpha * A
    b1 = -2.0 * cos(w0)
    b2 =  1.0 - alpha * A
    a0 =  1.0 + alpha / A
    a1 = -2.0 * cos(w0)
    a2 =  1.0 - alpha / A
    # fmt: on
    return (b0, b1, b2), (a0, a1, a2)


# noinspection PyTypeChecker
def frequency_response(bands_coefs, graph_freqs):
    """return w, h frequency response components for all filters"""

    def band_response(band_coefs):
        b = band_coefs[0]
        a = band_coefs[1]
        _, h = signal.freqz(b, a, worN=graph_freqs, whole=True)
        return h

    # multiply all values in all bands frequency response
    bands_freq_response = reduce(numpy.multiply, list(map(band_response, bands_coefs)))

    return bands_freq_response


class ResponsePlot:
    def __init__(self, graph_height, graph_freqs):
        self.graph_freqs = graph_freqs
        self._mplFigure = Figure()
        self._mplFigureSetup = False
        self.mplCanvas = FigureCanvas(self._mplFigure)
        self.mplCanvas.setMaximumHeight(graph_height)
        self.mplCanvas.setMinimumHeight(graph_height)
        self._pltAx1 = None
        self._pltAx2 = None
        self._pltLine1 = None
        self._pltLine2 = None

    def setup_plt_fig(self, w, h):
        fig = self._mplFigure
        fig.patch.set_facecolor("#333333")
        self._pltAx1 = ax1 = fig.add_subplot(111)
        ax1.patch.set_facecolor("#111111")
        ax1.grid(color="#dddddd", linestyle="-")
        ax1.grid(which="minor", axis="x", color="#bbbbbb", linestyle="--")
        ax1.spines["bottom"].set_color("#dddddd")
        ax1.spines["top"].set_color("#dddddd")
        ax1.spines["right"].set_color("#dddddd")
        ax1.spines["left"].set_color("#dddddd")
        ax1.tick_params(axis="x", colors="#dddddd")
        ax1.tick_params(axis="y", colors="#dddddd")
        (self._pltLine1,) = self._pltAx1.plot(
            w, 20 * numpy.log10(abs(h)), "#00ff00", linewidth=2.0
        )
        # noinspection PyTypeChecker
        ax1.axis([10, SR / 2, -12, 12])
        ax1.set_ylabel("Amplitude [dB]", color="#00ff00")
        ax1.set_xscale("log")
        self._pltAx2 = ax2 = ax1.twinx()
        ax2.spines["bottom"].set_color("#dddddd")
        ax2.spines["top"].set_color("#dddddd")
        ax2.spines["right"].set_color("#dddddd")
        ax2.spines["left"].set_color("#dddddd")
        ax2.tick_params(axis="x", colors="#dddddd")
        ax2.tick_params(axis="y", colors="#dddddd")
        angles = numpy.unwrap(numpy.angle(h)) / pi * 180
        (self._pltLine2,) = self._pltAx2.plot(w, angles, color="#ff6666", zorder=5)
        ax2.set_ylabel("Phase [degrees]", color="#ff6666")
        ax2.grid(False)
        ax2.set_ylim(-90, 90)
        ax2.set_xlim(10, SR / 2)
        self._mplFigure.tight_layout(pad=0)
        self.mplCanvas.draw()

    def update_graph(self, frequency_response, gain):
        """update frequency response graph"""
        h = frequency_response
        w = self.graph_freqs * SR / pi * 0.5
        if not self._mplFigureSetup:
            self.setup_plt_fig(w, h)
            self._mplFigureSetup = True
        self._pltLine1.set_ydata(20 * numpy.log10(abs(h)))
        self._pltLine2.set_ydata(numpy.unwrap(numpy.angle(h)) / pi * 180)
        self._pltAx1.set_ylim(-12 + gain, 12 + gain)
        self.mplCanvas.draw()
