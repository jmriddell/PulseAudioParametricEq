import struct
import mmap
import os
import glob

from helpers.constants import PARAMEQ_MMAP_MAGIC


def _make_buffer(params):
    return struct.pack("fffffffffffffffff", 1.0, *params)


class LinkedEQ:
    def __init__(self):
        self._mmaps = []

    def setupMmaps(self):
        """setup mmaps"""
        os.chdir("/dev/shm")
        for fname in glob.glob(f"t5_3BandParamEqWithShelves_{PARAMEQ_MMAP_MAGIC}_*.*"):
            f = open(fname, "a+b")
            self._mmaps.append(mmap.mmap(f.fileno(), 0))

    def make_neutral(self):
        self.applyEq(
            (
                # fmt: off
                100.0, 0.0, 1.0,
                300.0, 0.0, 1.0,
                1000.0, 0.0, 1.0,
                3000.0, 0.0, 1.0,
                10000.0, 0.0, 1.0,
                0.0,
                # fmt: on
            )
        )

    def applyEq(self, ui_params):
        """apply ui settings to eq"""
        buffer = _make_buffer(ui_params)
        for mm in self._mmaps:
            mm.seek(0)
            mm.seek(0)
            mm.write(buffer)
