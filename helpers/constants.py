import math
import os
from numpy import array

CURRENT_VERSION = "1.7"

DIALSTEPS = 10000

FREQUENCY_DEFAULTS = {
    "Low": 100,
    "Param1": 330,
    "Param2": 1000,
    "Param3": 3300,
    "High": 10000,
}

GRAPH_FREQUS = []
_f = 0.001
while _f < math.pi:
    GRAPH_FREQUS.append(_f)
    _f = _f * 1.03
GRAPH_FREQUS = array(GRAPH_FREQUS)

GRAPH_HEIGHT = 250

# order matters for MainWindow._uiParams
PARAM_TYPES = [
    "Frequency",
    "Gain",
    "Q",
]

# order matters for MainWindow._uiParams
BAND_NAMES = [
    "Low",
    "Param1",
    "Param2",
    "Param3",
    "High",
]

LADSPA_SINK_NAME = "PulseaudioParamEq.Input"
LADSPA_LABEL = "3band_parameq_with_shelves"
LADSPA_LIBRARY = "t5_3band_parameq_with_shelves"

MAX_FREQ = 20000.0
MIN_FREQ =    10.0
MINMAX_FREQ_RATIO_LOG2 = math.log(MAX_FREQ/MIN_FREQ, 2)

MIN_GAIN =   -12.0
MAX_GAIN =    12.0

PARAMEQ_MMAP_MAGIC = 12345678

PA_CLIENT_NAME = "PaParamEq-%s" % os.getpid()

SR = 44100 # Simulated samplerate for drawing frequency responses
