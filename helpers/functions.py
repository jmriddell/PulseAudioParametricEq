from helpers.constants import *

try:
    import pulsectl
except ImportError:
    pulsectl = None  # the import error is handled in MainWindow.py


_PULSE = None


def dialSteps2frequency(dialSteps):
    """convert dial steps to frequency"""
    ratio = dialSteps / DIALSTEPS * MINMAX_FREQ_RATIO_LOG2
    return int(round(math.pow(2, ratio) * MIN_FREQ))


def dialSteps2gain(dialSteps):
    """convert dial steps to gqin"""
    return MIN_GAIN + dialSteps / DIALSTEPS * (MAX_GAIN - MIN_GAIN)


def dialSteps2q(dialSteps):
    """convert dial steps to q"""
    return math.pow(10, -1 + dialSteps / (DIALSTEPS / 2))


def frequency2dialSteps(hz):
    """convert frequency to dial steps"""
    ratio = (MAX_FREQ - MIN_FREQ) / hz
    ret = (MINMAX_FREQ_RATIO_LOG2 - math.log(ratio, 2)) / MINMAX_FREQ_RATIO_LOG2
    return int(round(ret * DIALSTEPS))


def gain2dialSteps(gain):
    """convert gain to dial steps"""
    ret = int(round(DIALSTEPS / 2 + gain / (MAX_GAIN - MIN_GAIN) * DIALSTEPS))
    return ret


def q2dialSteps(q):
    """convert q to dial steps"""
    return int(round((DIALSTEPS / 2) + math.log(q, 10) * (DIALSTEPS / 2)))


def pulse_get_default_sink():
    """return default sink name"""
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        return _PULSE.server_info().default_sink_name
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        return ""


def pulse_get_hw_sinks():
    """return hardware sink names/descriptions"""
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        ret = []
        for _sinkinfo in _PULSE.sink_list():
            if _sinkinfo.proplist.get("device.master_device", None) is None:
                ret.append(
                    {"name": _sinkinfo.name, "description": _sinkinfo.description}
                )
        return ret
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        return []


def pulse_get_sink_volume(sink_name):
    """return sink volume as float"""
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        for _sinkinfo in _PULSE.sink_list():
            if _sinkinfo.name == sink_name:
                return _sinkinfo.volume.value_flat
    except pulsectl.pulsectl.PulseError:
        _PULSE = None
        return 0.0


def pulse_set_sink_volume(sink_name, volume):
    """set volume from float"""
    global _PULSE
    try:
        if _PULSE is None:
            _PULSE = pulsectl.Pulse(PA_CLIENT_NAME)
        for _sinkinfo in _PULSE.sink_list():
            if _sinkinfo.name == sink_name:
                _PULSE.volume_set_all_chans(_sinkinfo, volume)
    except pulsectl.pulsectl.PulseError:
        _PULSE = None


def pulse_move_sink_inputs(sink_name):
    cmd = (
        """pacmd "list-sink-inputs" | grep 'index: ' | sed 's/    index: /pacmd move-sink-input /g' | sed 's/$/ %s/g'"""
        % sink_name
    )
    ret = os.popen(cmd).read().strip("\n")
    cmd = ret.replace("\n", "; ")
    os.system(cmd)


def pulse_load_eq_module(sink_name, sink_master):
    cmd = "pactl load-module module-ladspa-sink "
    cmd += "sink_name=%s " % sink_name
    cmd += "sink_master=%s " % sink_master
    cmd += "plugin=t5_3band_parameq_with_shelves label=3band_parameq_with_shelves "
    cmd += (
        "control=100,0,1,300,0,1,1000,0,1,3000,0,1,10000,0,1,0,%s" % PARAMEQ_MMAP_MAGIC
    )
    os.system(cmd)


def pulse_set_default_sink(sink_name):
    cmd = "pactl set-default-sink %s" % sink_name
    os.system(cmd)


def pulse_insert_module(sink_name, sink_master):
    pulse_load_eq_module(sink_name, sink_master)
    pulse_set_default_sink(sink_name)
    pulse_move_sink_inputs(sink_name)


VALUE_TO_TEXT_FUNCTIONS = {
    "Frequency": lambda x: "%d Hz" % dialSteps2frequency(x),
    "Gain": lambda x: "%0.1f dB" % dialSteps2gain(x),
    "Q": lambda x: "%0.2f" % dialSteps2q(x),
}

TEXT_TO_VALUE_FUNCTIONS = {
    "Frequency": lambda x: max(MIN_FREQ, min(MAX_FREQ, int(x.split(" ")[0]))),
    "Gain": lambda x: max(-20.0, min(20.0, float(x.split(" ")[0]))),
    "Q": lambda x: max(0.1, min(10.0, float(x))),
}

VALUE_TO_DIAL_STEPS_FUNCTIONS = {
    "Frequency": frequency2dialSteps,
    "Gain": gain2dialSteps,
    "Q": q2dialSteps,
}
