from importlib import import_module
from subprocess import CalledProcessError, check_call
import sys
import os

from PyQt5.QtWidgets import QMessageBox


def install_package(package):
    check_call([sys.executable, "-m", "pip", "install", package])


def qt_app_import_check(main_window, module_name):
    # TODO: remove qt dependencies and use custom exceptions instead
    try:
        import_module(module_name)
    except ImportError:
        dlg = QMessageBox()
        msg = "WARNING:\n"
        msg += f"  Could not find python module '{module_name}'.\n"
        msg += "  Should we try to install it for you?\n"
        msg += "  (This may take a few seconds...)"
        result = dlg.warning(
            main_window, "Dependencies warning", msg, QMessageBox.Yes | QMessageBox.No
        )
        if result == QMessageBox.Yes:
            try:
                install_package(module_name)
            except CalledProcessError:
                msg = f"ERROR: module '{module_name}' not found.\n"
                msg += f"  Manual installation: '{sys.executable} -m pip install {module_name}'"
                QMessageBox().critical(main_window, "ERROR loading dependencies!", msg)
                print(msg, file=sys.stderr)
                sys.exit(1)
            dlg = QMessageBox()
            msg = "SUCCESS:\n"
            msg += f"  The {module_name} module was successfully installed.\n"
            msg += "  The program will be restarted now..."
            dlg.information(main_window, "Need to restart...", msg, QMessageBox.Ok)
            os.execv(sys.executable, [sys.executable] + sys.argv)
        else:
            sys.exit(1)
