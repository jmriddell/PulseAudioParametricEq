# Pulseaudio Parametric Equalizer

A fully parametric equalizer to insert into the pulseaudio sound system with a QT-GUI.

Please see the webpage at 
https://t-5.eu/hp/Software/Pulseaudio%20Parametric%20Equalizer/
for more information...